<#macro myLayout title="Pages">

    <#global logoPath>/img/logo.png</#global>
    <#global logoPathRetina>/img/logo_2x.png</#global>

    <#global whiteLogoPath>/img/logo_white.png</#global>
    <#global whiteLogoPathRetina>/img/logo_white_2x.png</#global>

    <#assign logoUrl>pages/ico</#assign>

<!DOCTYPE html>
<html>
<head>
    <title>${title}</title>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <link rel="apple-touch-icon" href="${logoUrl}/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="${logoUrl}/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="${logoUrl}/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="${logoUrl}/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico"/>

<#-- ============== CSS =================-->
    <#include "css-files.ftl"/>

</head>

<body class="fixed-header">

    <#nested/>

<#-- ============== JS =================-->
    <#include "js-files.ftl"/>
</body>
</html>
</#macro>