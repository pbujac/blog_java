<link href="${springMacroRequestContext.contextPath}/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
<link href="${springMacroRequestContext.contextPath}/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${springMacroRequestContext.contextPath}/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="${springMacroRequestContext.contextPath}/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
<link href="${springMacroRequestContext.contextPath}/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
<link href="${springMacroRequestContext.contextPath}/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="${springMacroRequestContext.contextPath}/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
<link href="${springMacroRequestContext.contextPath}/plugins/bootstrap-tag/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<link href="${springMacroRequestContext.contextPath}/plugins/dropzone/css/dropzone.css" rel="stylesheet" type="text/css" />
<link href="${springMacroRequestContext.contextPath}/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
<link href="${springMacroRequestContext.contextPath}/plugins/summernote/css/summernote.css" rel="stylesheet" type="text/css" media="screen">
<link href="${springMacroRequestContext.contextPath}/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" media="screen">
<link href="${springMacroRequestContext.contextPath}/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" media="screen">
<link href="${springMacroRequestContext.contextPath}/plugins/jquery-datatable/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
<link href="${springMacroRequestContext.contextPath}/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css" rel="stylesheet" type="text/css" />
<link href="${springMacroRequestContext.contextPath}/plugins/datatables-responsive/css/datatables.responsive.css" rel="stylesheet" type="text/css" media="screen" />

<link href="${springMacroRequestContext.contextPath}/css/pages-icons.css" rel="stylesheet" type="text/css">
<link class="main-stylesheet" href="${springMacroRequestContext.contextPath}/css/pages.css" rel="stylesheet" type="text/css" />
<!-- Include Editor style. -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_style.min.css" rel="stylesheet" type="text/css" />
