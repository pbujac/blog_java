<script src="${springMacroRequestContext.contextPath}/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/modernizr.custom.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javasFcript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/jquery-bez/jquery.bez.min.js"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/jquery-actual/jquery.actual.min.js"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/classie/classie.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/jquery-autonumeric/autoNumeric.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/dropzone/dropzone.min.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/bootstrap-tag/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/jquery-inputmask/jquery.inputmask.min.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/boostrap-form-wizard/js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/summernote/js/summernote.min.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/moment/moment.min.js"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
<!-- END VENDOR JS -->
<script src="${springMacroRequestContext.contextPath}/plugins/jquery-datatable/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js" type="text/javascript"></script>
<script type="text/javascript" src="${springMacroRequestContext.contextPath}/plugins/datatables-responsive/js/datatables.responsive.js"></script>
<script type="text/javascript" src="${springMacroRequestContext.contextPath}/plugins/datatables-responsive/js/lodash.min.js"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="${springMacroRequestContext.contextPath}/js/pages.min.js"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="${springMacroRequestContext.contextPath}/js/datatables.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL JS -->
<script src="${springMacroRequestContext.contextPath}/js/form_elements.js" type="text/javascript"></script>
<script src="${springMacroRequestContext.contextPath}/js/scripts.js" type="text/javascript"></script>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0//js/froala_editor.pkgd.min.js"></script>
<script>
    $(function () {
        $('textarea').froalaEditor()
    });
</script>

<script>
    $(function()
    {
        $('#form-login').validate()
    })
</script>