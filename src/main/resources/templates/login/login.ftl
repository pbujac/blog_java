<#import "../layout/base.ftl" as layout>

<@layout.myLayout "Login Page">
<#-- ============== PAGE CONTAINER =================-->
<div class="login-wrapper ">

<#-- ============== LEFT - Login Background Pic =================-->
    <#include "login-background.ftl"/>

<#-- ============== RIGHT - CONTAINER =================-->
    <div class="login-container bg-white">

        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">

        <#-- ============== LOGO =================-->
            <img src="${logoPath}" alt="logo"
                 data-src="${logoPath}"
                 data-src-retina="${logoPathRetina}"
                 width="78"
                 height="22">

            <p class="p-t-35">Sign into your pages account</p>

        <#-- ============== LOGIN FORM =================-->
            <#include "login-form.ftl"/>

        </div>
    </div>
<#-- ============== END RIGHT - CONTAINER =================-->

</div>
</@layout.myLayout>