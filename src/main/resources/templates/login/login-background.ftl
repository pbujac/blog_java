<div class="bg-pic">

<#-- ============== BACKGROUND =================-->
    <img src="/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg"
         data-src="/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg"
         data-src-retina="/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" alt="" class="lazy">

<#-- ============== CAPTION =================-->
    <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
        <h2 class="semi-bold text-white">
            Pages make it easy to enjoy what matters the most in the life</h2>
        <p class="small">
            ALL RIGHTS RESERVED © 2017 - PAGES
        </p>
    </div>

</div>