<form id="form-login" action="/login" method="post" class="p-t-15" role="form">


<#-- ============== FORM ERROR =================-->
<#if RequestParameters.error?? >
    <div class="has-error">
        <p class="text-danger">Email or Password invalid, please verify</p>
    </div>
</#if>

<#-- ============== USERNAME =================-->
    <div class="form-group form-group-default required">
        <label>Login</label>
        <div class="controls">
            <input type="email" name="email" placeholder="Email" class="form-control" required>
        </div>
    </div>

<#-- ============== PASSWORD =================-->
    <div class="form-group form-group-default required ">
        <label>Password</label>
        <div class="controls">
            <input type="password" class="form-control" name="password" placeholder="Credentials" required>
        </div>
    </div>

<#-- ============== SUBMIT =================-->
    <button class="btn btn-primary btn-cons m-t-10" type="submit">Sign in</button>
</form>