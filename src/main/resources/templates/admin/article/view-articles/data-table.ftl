<div class="container-fluid container-fixed-lg bg-white">

<#-- ============== TITLE HEADING =================-->
    <div class="panel panel-transparent">
        <div class="panel-heading">
            <div class="panel-title">Pages Default Tables Style
            </div>
            <div class="pull-right">
                <div class="col-xs-12">
                    <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

    <#-- ============== TABLE =================-->
        <div class="panel-body">
            <table class="table table-hover demo-table-search" id="tableWithSearch">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Places</th>
                    <th>Activities</th>
                    <th>Status</th>
                    <th>Last Update</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="v-align-middle semi-bold">
                        <p>First Tour</p>
                    </td>
                    <td class="v-align-middle">
                        <a href="#" class="btn btn-tag">United States</a>
                        <a href="#" class="btn btn-tag">India</a>
                    </td>
                    <td class="v-align-middle">
                        <p>it is more then ONE nation/nationality as its fall name is The United Kingdom of Great
                            Britain and North Ireland..</p>
                    </td>
                    <td class="v-align-middle">
                        <p>Public</td>
                    <td class="v-align-middle">
                        <p>April 13,2014 10:13</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>