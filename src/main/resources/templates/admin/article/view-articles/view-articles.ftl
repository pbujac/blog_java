<#import "../../layout/base-admin.ftl" as layout>

<@layout.userLayout "ADMIN - View article">

<div class="container-fluid container-fixed-lg">

<#-- ============== PAGE LOCATION =================-->
    <#include "page-location.ftl"/>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-transparent">
                <div class="panel-body">

                <#-- ============== DATA TABLE=================-->
    <#include "data-table.ftl"/>
                </div>
            </div>
        </div>
    </div>

</div>

</@layout.userLayout>
