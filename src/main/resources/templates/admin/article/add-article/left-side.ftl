<#-- ============== TITLE =================-->
<div class="col-md-8">
    <div class="form-group form-group-default">
        <label>Title</label>
        <input type="text" class="form-control" placeholder="Please insert title here..." name="title"/>
    </div>

<#-- ============== CATEGORY =================-->

    <div class="form-group form-group-default form-group-default-select2 required">
        <label class="">Category</label>
        <select class="full-width" data-placeholder="Select category" data-init-plugin="select2">

                <option value="AK">Alaska</option>
                <option value="HI">Hawaii</option>
                <option value="CA">California</option>
                <option value="NV">Nevada</option>

        </select>
    </div>

<#-- ============== LANGUAGE =================-->

    <div class="form-group form-group-default form-group-default-select2 required">
        <label class="">Language</label>
        <select class="full-width" data-placeholder="Select category" data-init-plugin="select2">

            <option value="AK">Romanian</option>
            <option value="HI">Russian</option>
            <option value="CA">English</option>

        </select>
    </div>

<#-- ============== CATEGORY =================-->

    <div class="form-group form-group-default form-group-default-select2 required">
        <label class="">Category</label>
        <select class="full-width" data-placeholder="Select category" data-init-plugin="select2">

            <option value="AK">Alaska</option>
            <option value="HI">Hawaii</option>
            <option value="CA">California</option>
            <option value="NV">Nevada</option>

        </select>
    </div>


<#-- ============== AUTHORS =================-->

    <div class="form-group form-group-default form-group-default-select2">
        <label>Author</label>
        <select class=" full-width" data-init-plugin="select2" multiple>
            <option value="Jim">Jim</option>
            <option value="John">John</option>
            <option value="Lucy">Lucy</option>
        </select>
    </div>


<#-- ============== DATE =================-->

    <div class="form-group form-group-default input-group col-sm-12">
        <label>Date To Publish</label>
        <input type="email" class="form-control" placeholder="Pick a date" id="datepicker-component2">
        <span class="input-group-addon">
                                                  <i class="fa fa-calendar"></i>
                                                </span>
    </div>
</div>