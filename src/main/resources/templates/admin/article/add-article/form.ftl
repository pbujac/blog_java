<form id="form-personal" role="form" autocomplete="off">
    <div class="row">

        <#-- ============== LEFT SIDE - HEADER DATA =================-->
        <#include "left-side.ftl"/>

        <#-- ============== RIGHT SIDE - SUBMIT DATA =================-->
        <#include "right-side.ftl"/>

         <#-- ============== RIGHT SIDE - SUBMIT DATA =================-->
        <#include "text-editor.ftl"/>

    </div>

</form>
