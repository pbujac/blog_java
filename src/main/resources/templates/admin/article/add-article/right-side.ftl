<div class="col-md-4">

    <div class="form-group form-group-default">
        <label>Actions</label>
    <#-- ============== SUBMIT - ADD ARTICLE =================-->
        <button style="margin: 10px 3px" class="btn btn-complete" type="submit">Save</button>

    <#-- ============== SUBMIT - ADD ARTICLE =================-->
        <button style="margin: 10px 3px" class="btn btn-success" type="submit">Save and close</button>
    </div>

<#-- ============== STATUS =================-->
    <div class="form-group form-group-default form-group-default-select2 required">
        <label class="">Status</label>
        <select class="full-width" data-placeholder="Select category" data-init-plugin="select2">

            <option value="AK">New</option>
            <option value="HI">Hawaii</option>
            <option value="CA">California</option>
            <option value="NV">Nevada</option>

        </select>
    </div>

<#-- ============== SWITHECS =================-->
    <div class="form-group form-group-default">
        <label class="">Switches</label>

        <div class="checkbox check-primary checkbox-circle">
            <input type="checkbox" checked="checked" value="1" id="checkbox9">
            <label for="checkbox9">Show on carousel</label>
        </div>
    </div>

<#-- ============== TAGS =================-->
    <div class="form-group form-group-default form-group-default-select2">
        <label>Tags</label>
        <select class=" full-width" data-init-plugin="select2" multiple>
            <option value="Jim">Fashion</option>
            <option value="John">Beauty</option>
            <option value="Lucy">Man</option>
        </select>
    </div>

<#-- ============== MEDIA =================-->
    <div class="form-group form-group-default">
        <label>Media</label>

        <button style="margin: 10px 3px" class="btn btn-primary" type="submit">Show images</button>
        <button  style="margin: 10px 3px" class="btn btn-complete" type="submit">Add new images</button>
    </div>


</div>

