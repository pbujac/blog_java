<div class="sidebar-header">

<#-- ============== LOGO =================-->
    <img src="${whiteLogoPath}" alt="logo" class="brand"
         data-src="${whiteLogoPath}"
         data-src-retina="${whiteLogoPathRetina}"
         width="78" height="22"/>

<#-- ============== HEADER CONTROLS=================-->
    <div class="sidebar-header-controls">
        <button type="button" class="btn btn-link sidebar-slide-toggle visible-lg-inline" data-toggle-pin="sidebar" data-pages-toggle="#sidebar">
            <i class="fa fa-angle-down fs-16"></i>
        </button>
    </div>
</div>