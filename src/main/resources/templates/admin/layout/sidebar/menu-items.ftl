<div class="sidebar-menu">

    <ul class="menu-items">
    <#-- ============== HOME ITEM ================-->
        <li class="m-t-30 ">
            <a href="/admin/home" class="detailed">
                <span class="title">Home</span>
                <#--<span class="details">12 New Updates</span>-->
            </a>
            <span class="icon-thumbnail bg-success"><i class="pg-home"></i></span>
        </li>
    <#-- ============== MULTI SELECT ITEMS =================-->
        <li class="">
            <a href="javascript:;"><span class="title">Article</span>
                <span class=" arrow"></span></a>
            <span class="icon-thumbnail">Ui</span>
            <ul class="sub-menu">
                <li class="">
                    <a href="/add-article">Add article</a>
                    <span class="icon-thumbnail">c</span>
                </li>
                <li class="">
                    <a href="/view-articles">View Articles</a>
                    <span class="icon-thumbnail">t</span>
                </li>
            </ul>
        </li>

    </ul>
    <div class="clearfix"></div>
</div>