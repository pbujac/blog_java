<div class="header ">

<#-- ============== MOBILE CONTROLS =================-->
<#include "mobile-controls.ftl"/>

<#-- ============== LOGO=================-->
    <div class=" pull-left sm-table">
        <div class="header-inner">
        <#include "logo.ftl"/>

            <a href="#" class="search-link" data-toggle="search">
                <i class="pg-search"></i>
                Type anywhere to <span class="bold">search</span>
            </a>
        </div>
    </div>

<#-- ============== USER INFO =================-->
<#include "../nav/user-info.ftl"/>
</div>