<div class="brand inline">
    <img src="${logoPath}" alt="logo"
         data-src="${logoPath}"
         data-src-retina="${logoPathRetina}"
         width="78" height="22">
</div>
