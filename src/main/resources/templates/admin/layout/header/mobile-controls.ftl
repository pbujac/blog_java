<#-- ============== LEFT SIDE =================-->
<div class="pull-left full-height visible-sm visible-xs">
<#-- ============== ACTION BAR =================-->
    <div class="sm-action-bar">
        <a href="#" class="btn-link toggle-sidebar" data-toggle="sidebar">
            <span class="icon-set menu-hambuger"></span>
        </a>
    </div>
</div>
