<#import "../../layout/base.ftl" as layout>

<#assign title>ADMIN - HomePage</#assign>

<#macro userLayout title="${title}">

    <@layout.myLayout "${title}">

    <#-- ============== PAGE SIDEBAR =================-->
        <#include "sidebar/base.ftl"/>

    <#-- ============== PAGE CONTAINER =================-->
    <div class="page-container">
    <#-- ============== HEADER =================-->
        <#include "header/header.ftl"/>

    <#-- ============== CONTENT =================-->
        <div class="page-content-wrapper">
            <div class="content">
                <#nested/>
            </div>
        <#-- ============== FOOTER=================-->
            <#include "footer/footer.ftl"/>
        </div>
    </div>
    </@layout.myLayout>
</#macro>