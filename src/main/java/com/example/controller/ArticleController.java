package com.example.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ArticleController {


    @RequestMapping(value = "/add-article", method = RequestMethod.GET)
    public ModelAndView addArticleAction() {
        return new ModelAndView("admin/article/add-article/add-article");
    }

    @RequestMapping(value = "/view-articles", method = RequestMethod.GET)
    public ModelAndView viewqArticlesAction() {
        return new ModelAndView("admin/article/view-articles/view-articles");
    }


}
