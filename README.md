Blog Template 
===========
A blog website

## Built With

* [Java] - The programming language used
* [Spring Boot] 
* [Spring security]
* [Hibernate]

## Run project
Make sure you have installed Java > 7, MySQL


-   Import db from resources/sql
-   Used "pages" template for UI  (CSS)


Open project and run in terminal:
```
1. mvn clean
2. mvn clean install
3. mvn spring-boot:run
```
## Screenshots

### Articles pages
![Alt text](screenshots/img01.png?raw=true "Articles Page")

### Edit page
![Alt text](screenshots/img02.png?raw=true "Edit Page")
